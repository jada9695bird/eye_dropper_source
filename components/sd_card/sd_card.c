
#include "sd_card.h"

static const char *TAG = "sd_card";

// working SD card filenames
RTC_DATA_ATTR char cplog_filename[30];
RTC_DATA_ATTR char prlog_filename[30];
RTC_DATA_ATTR char unsnt_filename[30];


/*  This function sets up SPI protocol and mounts the SD card
	If the SD card fails to mount, it will be formatted
*/
void sd_card_init(void)
{
    ESP_LOGI(TAG, "Initializing SD card");

    ESP_LOGI(TAG, "Using SPI peripheral");
	

    sdmmc_host_t host = SDSPI_HOST_DEFAULT();
    sdspi_slot_config_t slot_config = SDSPI_SLOT_CONFIG_DEFAULT();
    slot_config.gpio_miso = PIN_NUM_MISO;
    slot_config.gpio_mosi = PIN_NUM_MOSI;
    slot_config.gpio_sck  = PIN_NUM_CLK;
    slot_config.gpio_cs   = PIN_NUM_CS;
    // This initializes the slot without card detect (CD) and write protect (WP) signals.
    // Modify slot_config.gpio_cd and slot_config.gpio_wp if your board has these signals.

    // Options for mounting the filesystem.
    // If format_if_mount_failed is set to true, SD card will be partitioned and
    // formatted in case when mounting fails.
    esp_vfs_fat_sdmmc_mount_config_t mount_config = {
        .format_if_mount_failed = false,
        .max_files = 5,
        .allocation_unit_size = 16 * 1024
    };

    // Use settings defined above to initialize SD card and mount FAT filesystem.
    // Note: esp_vfs_fat_sdmmc_mount is an all-in-one convenience function.
    // Please check its source code and implement error recovery when developing
    // production applications.
    sdmmc_card_t* card;
    esp_err_t ret = esp_vfs_fat_sdmmc_mount("/sdcard", &host, &slot_config, &mount_config, &card);

    if (ret != ESP_OK) {
        if (ret == ESP_FAIL) {
            ESP_LOGE(TAG, "Failed to mount filesystem. "
                "If you want the card to be formatted, set format_if_mount_failed = true.");
        } else {
            ESP_LOGE(TAG, "Failed to initialize the card (%s). "
                "Make sure SD card lines have pull-up resistors in place.", esp_err_to_name(ret));
        }
        return;
    }
	
	// Card has been initialized, print its properties
    sdmmc_card_print_info(stdout, card);
}


// Generate three files that contain:
// 		- a log of all cap turns (CPLOG)
//		- a log of system events (PRLOG)
//		- a log of unsent data (UNSNT)
// Note: It is possible that during a brownout the SD card is wiped/reset by this function.
//		- TODO - find an elegant fix
void sd_create_new_file(){
	
	// generate a file structure that does not overwrite previous data on the card
	generate_filesystem();
	
	//CPLOG
	ESP_LOGI(TAG, "Creating Cap Log file");
    FILE* f1 = fopen(cplog_filename, "w");
    if (f1 == NULL) {
        ESP_LOGE(TAG, "Failed to open file for writing");
        return;
    }
    fprintf(f1, "Log of all cap turn events\r\n");
    fclose(f1);
    ESP_LOGI(TAG, "File written");
	//PRLOG
	ESP_LOGI(TAG, "Creating Process Log file");
    FILE* f2 = fopen(prlog_filename, "w");
    if (f2 == NULL) {
        ESP_LOGE(TAG, "Failed to open file for writing");
        return;
    }
    fprintf(f2, "Log of all system process events\r\n");
    fclose(f2);
    ESP_LOGI(TAG, "File written");
	//UNSNT
	ESP_LOGI(TAG, "Creating Unsent Log file");
    FILE* f3 = fopen(unsnt_filename, "w");
    if (f3 == NULL) {
        ESP_LOGE(TAG, "Failed to open file for writing");
        return;
    }
    fprintf(f3, "Log of unsent cap turn events\r\n");
    fclose(f3);
    ESP_LOGI(TAG, "File written");
	
}


// Generate file system that does not overwrite previous data
// If a brownout occurs, old directories containing important data won't be overwritten
void generate_filesystem(){
	
	// for checking if directory exists
	// full path name
	char full_path[20] = "/sdcard/";
	char home[10] = "/sdcard/";
	memset(full_path,'\0',sizeof(full_path));
	// create session number
	int session_num = 0;
	char session_str[4];
	memset(session_str,'\0',sizeof(session_str));
	itoa(session_num, session_str, 10);
	// assemble initial path
	strcpy(full_path,home);
	strcat(full_path, session_str);
	// if the base directory exists, increment session number until finding an unused one
	// check directories to find next unique session number
	DIR* directory = opendir(full_path);
	while(directory!=NULL){
		session_num++;
		itoa(session_num, session_str, 10);
		strcpy(full_path,home);
		strcat(full_path, session_str);
		directory = opendir(full_path);
	}

	// make directory at the new path
	int status = mkdir(full_path, 0777);
	if(status!=-1){
		ESP_LOGI(TAG, "Successfully created new directory");
	}
	else{
		ESP_LOGI(TAG, "Error creating new directory");
	}
	
	// update static filenames
	strcpy(cplog_filename, full_path);
	strcat(cplog_filename,"/cplog.txt");
	strcpy(prlog_filename, full_path);
	strcat(prlog_filename,"/prlog.txt");
	strcpy(unsnt_filename, full_path);
	strcat(unsnt_filename,"/unsnt.txt");
}



// Read a given line from the unsent file
int sd_read_line_unsent(char* buffer, int line){
	
	// line operation is acting upon the nth line
	int n = 1;
	// open UNSNT.txt in read mode
	ESP_LOGI(TAG, "Opening file");
	FILE* f = fopen(unsnt_filename, "r");
	// call fgets 'line' number of times with the same buffer to read that line
	// the file pointer is automatically incremented by fgets
	while(n<=line){
		// read line and increment file pointer
		// if we get a NULL pointer back, return with exit code 0
		if(fgets(buffer,LINE_BUFFER_SIZE,f)==NULL){
			fclose(f);
			return 0;
		}
		n++;
	}
	fclose(f);
	// if all went well, return exit code 1
	return 1;
}


// Clear unsent file and re-write the top text
void sd_clear_unsent(){
	
	ESP_LOGI(TAG, "Opening file");
	//open log file and write to it
	FILE* f1 = fopen(unsnt_filename, "w");
	fprintf(f1, "Log of unsent cap turn events\r\n");
	fclose(f1);
	ESP_LOGI(TAG, "File appended");
}


// Log system process events in the SD card's PRLOG.txt file
void sd_log_process(char* timestamp, char* event_type, int need_to_send){
	
	//assemble data into buffer to be written
	ESP_LOGI(TAG, "assembling buffer");
	char* buffer = (char*)malloc(400*sizeof(char));
	if((timestamp!=NULL)&&(event_type!=NULL)){
		strcpy(buffer, timestamp);
		strcat(buffer, ",");
		strcat(buffer, event_type);
		strcat(buffer, "\r\n");
	}
	else{
		strcpy(buffer, "error\r\n");
	}
	ESP_LOGI(TAG, "Opening file");
	//open log file and write to it
	FILE* f = fopen(prlog_filename, "a");
	fprintf(f, buffer);
	fclose(f);
	ESP_LOGI(TAG, "File appended");
	//if we need to send this info to the site
	if(need_to_send){
		ESP_LOGI(TAG, "Opening file");
		//open log file and write to it
		FILE* f1 = fopen(unsnt_filename, "a");
		fprintf(f1, buffer);
		fclose(f1);
		ESP_LOGI(TAG, "File appended");
	}
	
	free(buffer);
}


// Log cap turn event in the SD card's CPLOG.txt file
void sd_log_event(char* timestamp, char* pretty_time, char* unique_id, char* event_type){
	
	//assemble data into buffer to be written
	ESP_LOGI(TAG, "assembling buffer");
	char* buffer = (char*)malloc(400*sizeof(char));
	if((timestamp!=NULL)&&(pretty_time!=NULL)&&(unique_id!=NULL)&&(event_type!=NULL)){
		strcpy(buffer, timestamp);
		strcat(buffer, ",");
		strcat(buffer, pretty_time);
		strcat(buffer, ",");
		strcat(buffer, unique_id);
		strcat(buffer, ",");
		strcat(buffer, event_type);
		strcat(buffer, "\r\n");
	}
	else{
		strcpy(buffer, "error\r\n");
	}
	// copy buffer to CPLOG
	ESP_LOGI(TAG, "Opening CPLOG");
	//open log file and write to it
	FILE* f1 = fopen(cplog_filename, "a");
	fprintf(f1, buffer);
	fclose(f1);
	ESP_LOGI(TAG, "File appended");
	// copy buffer to UNSNT
	ESP_LOGI(TAG, "Opening UNSNT");
	//open log file and write to it
	FILE* f2 = fopen(unsnt_filename, "a");
	fprintf(f2, buffer);
	fclose(f2);
	ESP_LOGI(TAG, "File appended");
	
	free(buffer);
}




