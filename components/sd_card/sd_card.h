

#include <stdio.h>
#include <string.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include "esp_err.h"
#include "esp_log.h"
#include "esp_vfs_fat.h"
#include "driver/sdmmc_host.h"
#include "driver/sdspi_host.h"
#include "sdmmc_cmd.h"


// Pin mapping for SPI
#define PIN_NUM_MISO 19
#define PIN_NUM_MOSI 23
#define PIN_NUM_CLK  18
#define PIN_NUM_CS   5

// Event types to to be logged in SD card
#define EVENT_CAP_TURN			0 	// UNSNT and CPLOG
#define EVENT_GOOD_CONNECTION 	1	// PRLOG
#define EVENT_BAD_CONNECTION	2	// UNSNT and PRLOG
#define EVENT_ALARM_TRIGGERED	3	// UNSNT and PRLOG
#define EVENT_FIRST_WAKEUP		4	// PRLOG


// Maximum size of a given line of text to be read
#define LINE_BUFFER_SIZE 500



void sd_card_init(void);
void sd_create_new_file();
void generate_filesystem();
int sd_read_line_unsent(char* buffer, int line);
void sd_clear_unsent();
void sd_log_event(char* timestamp, char* pretty_time, char* unique_id, char* event_type);
void sd_log_process(char* timestamp, char* event_type, int need_to_send);




