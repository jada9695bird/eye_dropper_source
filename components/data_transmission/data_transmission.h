#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_system.h"
#include "nvs_flash.h"

#include "esp_http_client.h"

#define SERVER "http://35.165.157.126:8080/capture"
#define SERVER_INSTRUCT "http://35.165.157.126:8080/updateAlarmTime"

#define RESPONSE_BUFFER_SIZE 100

esp_err_t _http_event_handler(esp_http_client_event_t *evt);
void http_POST(char* timestamp, char* pretty_time, char* unique_id, char* event_type);
void http_POST_get_instructions(char* unique_id, char* response_buffer, int response_buffer_size);