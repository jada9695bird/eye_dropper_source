
#include "data_transmission.h"

static const char *TAG = "HTTP_CLIENT";




esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
            if (!esp_http_client_is_chunked_response(evt->client)) {
                // Write out data
                // printf("%.*s", evt->data_len, (char*)evt->data);
            }

            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGD(TAG, "HTTP_EVENT_DISCONNECTED");
            break;
    }
    return ESP_OK;
}


// Post data to server
void http_POST(char* timestamp, char* pretty_time, char* unique_id, char* event_type){
	
	//assemble POST contents into buffer
	//char* buffer = (char*)malloc(400*sizeof(char));
	char buffer[500];
	if((timestamp!=NULL)&&(pretty_time!=NULL)&&(unique_id!=NULL)&&(event_type!=NULL)){
		strcpy(buffer, timestamp);
		strcat(buffer, "&");
		strcat(buffer, pretty_time);
		strcat(buffer, "&");
		strcat(buffer, unique_id);
		strcat(buffer, "&");
		strcat(buffer, event_type);
	}
	else{
		strcpy(buffer, "request=bad");
	}
	
	esp_http_client_config_t config = {
        .url = SERVER,
        .event_handler = _http_event_handler,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
	
	ESP_LOGI(TAG, "got into the post request function");
    esp_http_client_set_url(client, SERVER);
    esp_http_client_set_method(client, HTTP_METHOD_POST);
    esp_http_client_set_post_field(client, buffer, strlen(buffer));
    esp_err_t err = esp_http_client_perform(client);
    if (err == ESP_OK) {
        ESP_LOGI(TAG, "HTTP POST Status = %d, content_length = %d",
                esp_http_client_get_status_code(client),
                esp_http_client_get_content_length(client));
    } else {
        ESP_LOGE(TAG, "HTTP POST request failed: %s", esp_err_to_name(err));
    }
}


// Send a post request with ID
// The goal is to receive instructions for when to set alarms, daily checkin, periodicity, etc
// For starters, only retrieve alarm behavior
// response_buffer contains the message from the server
void http_POST_get_instructions(char* unique_id, char* response_buffer, int response_buffer_size){
	
	
	// post MAC address
	esp_http_client_config_t config = {
        .url = SERVER_INSTRUCT,
        .event_handler = _http_event_handler,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
	
	ESP_LOGI(TAG, "got into the post request function");
    esp_http_client_set_url(client, SERVER_INSTRUCT);
    esp_http_client_set_method(client, HTTP_METHOD_POST);
    esp_http_client_set_post_field(client, unique_id, strlen(unique_id));
    esp_err_t err = esp_http_client_perform(client);
    if (err == ESP_OK) {
        ESP_LOGI(TAG, "HTTP POST Status = %d, content_length = %d",
                esp_http_client_get_status_code(client),
                esp_http_client_get_content_length(client));
				// if all goes well, retrieve response_buffer
				esp_http_client_read(client, response_buffer, response_buffer_size);
    } else {
        ESP_LOGE(TAG, "HTTP POST request failed: %s", esp_err_to_name(err));
    }
}
