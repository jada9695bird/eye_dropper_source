

#include "esp_sleep.h"
#include <time.h>
#include "esp_log.h"
#include "esp32/ulp.h"
#include "esp_pm.h"
#include "driver/touch_pad.h"
#include "driver/adc.h"
#include "driver/rtc_io.h"
#include "soc/rtc_cntl_reg.h"
#include "soc/sens_reg.h"
#include "soc/rtc.h"
#include "lwip/err.h"
#include "apps/sntp/sntp.h"

#include <string.h>
#include <sys/time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_attr.h"
#include "esp_timer.h"
#include "nvs_flash.h"
#include "driver/ledc.h"

// Driving the piezo buzzer
#define LEDC_HS_TIMER          LEDC_TIMER_0
#define LEDC_HS_MODE           LEDC_HIGH_SPEED_MODE
#define LEDC_HS_CH0_GPIO       (12)
#define LEDC_HS_CH0_CHANNEL    LEDC_CHANNEL_0
#define LEDC_TEST_DUTY         (2000)


// STATES
#define INITIAL_WAKE 		0	// first wake-up of the device
#define SLEEP		 		1	// hibernation
#define CAP_DETECT	 		2	// device was awoken by a cap turn
#define TIMED_RECONNECT 	3	// device was awoken by rtc
#define WIFI_GOOD			4	// successful reconnection to WiFi
#define WIFI_BAD			5	// unsuccessful reconnection to WiFi
#define ENGAGE_ALARM		6	// time to set of an alarm
#define WIFI_RESET			7	// re-initiate smart connect
#define EN_CAP_DETECT		8	// re-enable cap detect (typically 30 minutes after a cap detect occurs)
#define TIMED_CLOCK_RESET	9	// midpoint wake-up


// Sleep and States
void go_to_sleep(void);
void wake_up_routine(void);
int current_state(void);
int current_state();
void set_current_state(int state);
void set_future_state(int state);

// Time
void init_sleep_time();
void buzzer_config();
uint64_t get_sec_next_wake();
time_t select_next_wake_time();
void set_check_in_time(time_t t);
time_t set_check_in_phase(int hour, int minute);
void set_alarm_time(time_t t);
time_t set_alarm_phase(int hour, int minute);
int get_current_time_s();
void get_current_time_pretty(char* destination);
void calibrate_time(void);
void obtain_time(void);
void initialize_sntp(void);