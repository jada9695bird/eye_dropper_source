


#include "sleep_time.h"

// VARIABLES STORED IN RTC MEMORY

// states
RTC_DATA_ATTR int system_state;
RTC_DATA_ATTR int system_state_fut;

// next upcoming check in time 
// both of these represent seconds since 00:00 Jan 1, 1970
// must be incremented periodically
RTC_DATA_ATTR time_t check_in_time_t;
// time and minute of the day that checkin occurs
// either daily or as an initial checkin
RTC_DATA_ATTR int check_in_hour;
RTC_DATA_ATTR int check_in_min;

// alarm times - organized the same way as check-in time
RTC_DATA_ATTR time_t alarm_time_t;
RTC_DATA_ATTR time_t alarm_hour;
RTC_DATA_ATTR time_t alarm_min;

// STATIC CONST
static const char *TAG = "sleep_time";
// period of time between check-ins (seconds)
static const time_t check_in_period = 43200;
// period of time between alarms (seconds)
static const time_t alarm_period_long = 86400;
// frequency of beeps and/or LED flashes (seconds)
static const time_t alarm_period_short = 180;



static void oneshot_timer_callback(void* arg);

/*
	C Time variable reference:
	time_t 		 				- # seconds since 00:00 Jan 1, 1970
	struct tm   				- nine integer values coresponding with seconds, minutes, hours, etc
	struct timeval 				- contains # seconds (time_t) and # microseconds (long int) since 00:00 Jan 1, 1970
	----------------------------------------------------------------------------------------------------------------
	Time function reference:
	time(&time_t)				- get current system time
	localtime_r(&time_t, &tm)	- convert calendar time into tm struct
*/





// initialize time values stored in RTC
// these should be reset by during the initial connection with the server
// default - check in will be 9:00 pm if the patient hasn't taken the drops
// TODO - reset check in times according to instructions from server
void init_sleep_time(){
	
	ESP_LOGI(TAG, "Setting default check in and alarm times");
	// default check in time - the time after which taking a drop is non-adherant
	time_t checkin = set_check_in_phase(5,0);
	// default alarm time - the first alarm
	time_t alarmin = set_alarm_phase(2, 0);
	set_check_in_time(checkin);
	set_alarm_time(alarmin);
	ESP_LOGI(TAG, "Done setting up check in times");
}


// set the hour/minute of the day for the first check-in
//		hours 	= 0-23
//		minutes = 0-59
// check_in_period will be hardcoded for now
// time must be calibrated before calling this function
// TODO - link with server so we can set this from the web instead of hardcoded
time_t set_check_in_phase(int hour, int minute){
	
	//current time
	time_t now;
	time_t result;
    struct tm timeinfo;
    time(&now);
    localtime_r(&now, &timeinfo);
	
	// set hours and minutes to those specified and convert to time_t
	timeinfo.tm_hour = hour;
	timeinfo.tm_min = minute;
	// resultant time
	result = mktime(&timeinfo);
	
	// if next checkin of the day is prior to now, increase by check_in_period until it isn'takes
	while(result<=now){
		result+=check_in_period;
	}
	
	
	ESP_LOGI(TAG, "Check in phase starts: %lld", (long long)result);
	
	return result;
}


// set the hour/minute of the day for the first alarm
//		hours 	= 0-23
//		minutes = 0-59
// alarm_period will be hardcoded for now
// time must be calibrated before calling this function
// TODO - link with server so we can set this from the web instead of hardcoded
time_t set_alarm_phase(int hour, int minute){
	
	//current time
	time_t now;
	time_t result;
    struct tm timeinfo;
    time(&now);
    localtime_r(&now, &timeinfo);
	
	// set hours and minutes to those specified and convert to time_t
	timeinfo.tm_hour = hour;
	timeinfo.tm_min = minute;
	// resultant time
	result = mktime(&timeinfo);
	
	// if next alarm of the day is prior to now, increase by alarm period long until it isn't
	while(result<=now){
		result+=alarm_period_long;
	}
	
	
	ESP_LOGI(TAG, "Alarm phase starts: %lld", (long long)result);
	
	return result;
}


// This function is called every time the device goes into deep sleep mode
void go_to_sleep(void){
	
	// only set the timer if system clock has been initialized
	// TODO - make sure that any time wifi is not set, system_state_fut=WIFI_RESET
	if((system_state!=INITIAL_WAKE)&&(system_state_fut!=WIFI_RESET)){
		// calculate seconds until next wakeup
		// if next wakeup is alarm, set system_state_fut flag to ENGAGE_ALARM
		ESP_LOGI(TAG, "Enabling wakeup timers");
		uint64_t wakeup_time_sec = get_sec_next_wake();
		ESP_LOGI(TAG, "Enabling timer wakeup, %lld\n",(long long)wakeup_time_sec);
		esp_sleep_enable_timer_wakeup(wakeup_time_sec * (uint64_t)1000000);
	}
	
	// enable wakeup from external pin (IO34) to detect cap turn
	// enable wakeup from external pin (SENSOR_VN) to detect wifi reset
	const int ext_wakeup_pin_1 = 36;
	const int ext_wakeup_pin_2 = 34;
	const uint64_t ext_wakeup_pin_1_mask = 1ULL << ext_wakeup_pin_1;
	const uint64_t ext_wakeup_pin_2_mask = 1ULL << ext_wakeup_pin_2;
	// need to reset when either the wifi_reset button is pushed (low -> high) or the cap is removed (low -> high)
	// if we haven't initialized the system time yet, don't track cap turns
	// TODO - somehow invert button?
	//		- could temporarily fix using normally closed push button and a high(ish) value series resistor and much higher pull-up
	//		- however, this will continuously burn current - not a good long term solution
	if((system_state!=INITIAL_WAKE)&&(system_state_fut!=WIFI_RESET)&&(system_state!=CAP_DETECT)){
		esp_sleep_enable_ext1_wakeup(ext_wakeup_pin_1_mask | ext_wakeup_pin_2_mask, ESP_EXT1_WAKEUP_ANY_HIGH);
		ESP_LOGI(TAG, "Enabling all external resets");
	}
	else{
		//esp_sleep_enable_ext1_wakeup(ext_wakeup_pin_1_mask | ext_wakeup_pin_2_mask, ESP_EXT1_WAKEUP_ANY_HIGH);
		esp_sleep_enable_ext1_wakeup(ext_wakeup_pin_1_mask, ESP_EXT1_WAKEUP_ANY_HIGH);
		ESP_LOGI(TAG, "Enabling WIFI_RESET only");
	}
	
	esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_FAST_MEM, ESP_PD_OPTION_OFF);
	esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);
	
	// set the system state to sleep
	system_state = SLEEP;
	
	ESP_LOGI(TAG, "Going to sleep");
	
	// go to sleep
	esp_deep_sleep_start();
}





void buzzer_config(){
	
	 /*
     * Prepare and set configuration of timers
     * that will be used by LED Controller
     */
    ledc_timer_config_t ledc_timer = {
        .duty_resolution = LEDC_TIMER_13_BIT, // resolution of PWM duty
        .freq_hz = 2200,                      // frequency of PWM signal
        .speed_mode = LEDC_HS_MODE,           // timer mode
        .timer_num = LEDC_HS_TIMER            // timer index
    };
    // Set configuration of timer0 for high speed channels
    ledc_timer_config(&ledc_timer);
	
	ledc_channel_config_t ledc_channel = {
            .channel    = LEDC_HS_CH0_CHANNEL,
            .duty       = 0,
            .gpio_num   = LEDC_HS_CH0_GPIO,
            .speed_mode = LEDC_HS_MODE,
            .timer_sel  = LEDC_HS_TIMER
        };
	ledc_channel_config(&ledc_channel);
	
	ledc_set_duty(ledc_channel.speed_mode, ledc_channel.channel, LEDC_TEST_DUTY);
    ledc_update_duty(ledc_channel.speed_mode, ledc_channel.channel);
}





// If the timer times out, go back to sleep
static void oneshot_timer_callback(void* arg)
{
    ESP_LOGI(TAG, "Time is up, going back to sleep");
	vTaskDelay(2000 / portTICK_PERIOD_MS);
    go_to_sleep();
}


// The goal of this function is to establish the system state upon wakeup
// Additionally, a timer is started for 2 minutes after which the system will return to sleep
// This prevents the device from stalling from any given block of code. 
void wake_up_routine(void){

	// create the timer
	
	const esp_timer_create_args_t oneshot_timer_args = {
            .callback = &oneshot_timer_callback,
            //argument specified here will be passed to timer callback function
            .name = "one-shot"
    };
    esp_timer_handle_t oneshot_timer;
    ESP_ERROR_CHECK(esp_timer_create(&oneshot_timer_args, &oneshot_timer));
	// Start the timer (us)
	ESP_ERROR_CHECK(esp_timer_start_once(oneshot_timer, 120000000));
	
	
	// System indicator LED will be high while the device is awake
	gpio_pad_select_gpio(14);
	gpio_set_direction(14, GPIO_MODE_OUTPUT);
	gpio_set_level(14,1);
	gpio_pad_select_gpio(32);
	gpio_set_direction(32, GPIO_MODE_OUTPUT);
	gpio_set_level(32,0);
	
	// Turn on the buzzer
	buzzer_config();
	vTaskDelay(500 / portTICK_PERIOD_MS);
	
	
	// first check if this is the initial boot-up
	if(system_state==INITIAL_WAKE){
		//
		return;
	}
	
	// otherwise, establish the cause of the wake-up
	if(esp_sleep_get_wakeup_cause()==ESP_SLEEP_WAKEUP_TIMER){
		switch(system_state_fut){
			
			case ENGAGE_ALARM:
				system_state = ENGAGE_ALARM;
				break;
			case TIMED_RECONNECT:
				system_state = TIMED_RECONNECT;
				break;
			case CAP_DETECT:
				system_state = TIMED_RECONNECT;
				break;
			case TIMED_CLOCK_RESET:
				system_state = TIMED_CLOCK_RESET;
				break;
			case EN_CAP_DETECT:
				system_state = EN_CAP_DETECT;
				break;
			default:
				system_state = SLEEP;
				break;
		}
		}

	
	else if(esp_sleep_get_wakeup_cause()==ESP_SLEEP_WAKEUP_EXT1){
		ESP_LOGI(TAG, "Wake-up cause: external pin");
		// debounce and distinguish between cap detect and wifi reset states
		vTaskDelay(2000 / portTICK_PERIOD_MS);
		// check wifi reset button
		ESP_LOGI(TAG, "finished delay");
		gpio_pad_select_gpio(36);
		gpio_set_direction(36, GPIO_MODE_INPUT);
		if(!(gpio_get_level(36))){
			system_state = SLEEP;
		}
		else{
			system_state = WIFI_RESET;
			ESP_LOGI(TAG, "looks like a wifi reset");
		}
		// check cap detect sensor
		gpio_pad_select_gpio(34);
		gpio_set_direction(34, GPIO_MODE_INPUT);
		if((gpio_get_level(34))&&(system_state!=WIFI_RESET)){
			system_state = CAP_DETECT;
			ESP_LOGI(TAG, "looks like a cap detect");
		}
		
		ESP_LOGI(TAG, "System state = %d", system_state);
	}
	
	// set indicator LED high once we have gone through wake up logic
	gpio_set_level(14,0);
	
}

// increment check-in time by check-in period
// calculates the time between now and when the next wake time will be
// this function is used for both alarm behavior and check-ins
// do not increment by period if woke up from EXT1
uint64_t get_sec_next_wake(){
	
	/* system states
		INITIAL_WAKE 		0	// first wake-up of the device
		SLEEP		 		1	// hibernation
		CAP_DETECT	 		2	// device was awoken by a cap turn
		TIMED_RECONNECT 	3	// device was awoken by rtc
		WIFI_GOOD			4	// successful reconnection to WiFi
		WIFI_BAD			5	// unsuccessful reconnection to WiFi
		ENGAGE_ALARM		6	// time to set of an alarm
		WIFI_RESET			7	// re-initiate smart connect
		EN_CAP_DETECT		8	// re-enable cap detect (typically 30 minutes after a cap detect occurs)
		TIMED_CLOCK_RESET	9	// midpoint wake-up
	*/
	switch(system_state){
		
		//initial wake and sleep do not result in increment
		case TIMED_RECONNECT:
			set_check_in_time(check_in_time_t + check_in_period);
			break;
		case TIMED_CLOCK_RESET:
			set_check_in_time(check_in_time_t + check_in_period);
			break;
		case WIFI_GOOD:
			set_check_in_time(check_in_time_t + check_in_period);
			break;
		case WIFI_BAD:
			set_check_in_time(check_in_time_t + check_in_period);
			break;
		case CAP_DETECT:
			set_check_in_time(check_in_time_t + check_in_period);
			set_alarm_time((time_t)(alarm_time_t + alarm_period_long));
			break;
		case ENGAGE_ALARM:
			// increment alarm by short time to get periodic buzzes and blinks
			set_alarm_time((time_t)(alarm_time_t + alarm_period_short));
			break;
		default:
			break;
	}
	
	ESP_LOGI(TAG, "calculate next wake-up time");
	time_t now;
	uint64_t to_sleep;
    time(&now);
	// if we have just detected a cap, there is no need for alarms or check-ins in the same day
	if(system_state==CAP_DETECT){
		// 30 minute delay before a new cap turn can be recorded
		system_state_fut = EN_CAP_DETECT;
		// TODO - testing with different times
		to_sleep = 600;
	}
	else{
		time_t then = select_next_wake_time();
		to_sleep = (then - now);
	}
	
	
	return to_sleep;
}


// chooses the next upcoming time to wake up
time_t select_next_wake_time(){
	
	// if we have just detected a cap, there is no need for alarms or check-ins in the same day
	// otherwise...
	// the next check-in time occurs before the next alarms will go off, set timer for check-in or dummy check-in
	if(check_in_time_t<alarm_time_t){
		// future state will either be timed reconnect or timed clock reset
		if(system_state_fut==TIMED_RECONNECT)
			system_state_fut = TIMED_CLOCK_RESET;
		else
			system_state_fut = TIMED_RECONNECT;
		
		ESP_LOGI(TAG, "next wake-up during check-in");
		return check_in_time_t;
	}
	else{
		system_state_fut = ENGAGE_ALARM;
		return alarm_time_t;
	}
}


// set the value of check_in_time_t
// this will be used to update at least daily.
// input takes the form of a time_t
void set_check_in_time(time_t t){
	
	check_in_time_t = t;
}



// set the value of alarm_time_t
// This also assumes that alarms only occur on a daily basis
// input takes the form of a time_t
// TODO - more complex alarm behavior
void set_alarm_time(time_t t){
	
	alarm_time_t = t;
}


// Return the current state of the device
// Helps to abstract away memory, sleep, etc to this file
// instead of residing in main
int current_state(){
	
	return system_state;
}


// Set the current state
void set_current_state(int state){
	
	system_state = state;
}


// Set the future state
void set_future_state(int state){
	
	system_state_fut = state;
}



// Returns the current system time (unix timestamp) in ms
// If this is uninitialized, the value returned will be nonsensical
int get_current_time_s(){
	
	struct timeval now;
	gettimeofday(&now, NULL);
	int timeinseconds = now.tv_sec;
	return(timeinseconds);

}


// Returns the current system time in a human-readable format
// If this is uninitialized, the value returned will be nonsensical
void get_current_time_pretty(char* destination){
	
	if(destination==NULL){
		return;
	}
	//get the date and time
	time_t now;
	time(&now);
	//copy time into nice format
	strftime (destination,27,"%D/%R",gmtime(&now));
}


// 
void calibrate_time(void){
	
    time_t now;
    struct tm timeinfo;
    time(&now);
    localtime_r(&now, &timeinfo);
    // Is time set? If not, tm_year will be (1970 - 1900).
    if (timeinfo.tm_year < (2016 - 1900)) {
        ESP_LOGI(TAG, "Time is not set yet. Connecting to WiFi and getting time over NTP.");
        obtain_time();
        // update 'now' variable with current time
        time(&now);
    }
    char strftime_buf[64];
	//keep things in UTC, print time
    localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI(TAG, "The current date/time in New York is: %s", strftime_buf);

}


void obtain_time(void)
{
    initialize_sntp();

    // wait for time to be set
    time_t now = 0;
    struct tm timeinfo = { 0 };
    int retry = 0;
    const int retry_count = 10;
    while(timeinfo.tm_year < (2016 - 1900) && ++retry < retry_count) {
        ESP_LOGI(TAG, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        time(&now);
        localtime_r(&now, &timeinfo);
    }

}

void initialize_sntp(void)
{
    ESP_LOGI(TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_init();
}
