/* Main
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "driver/gpio.h"
#include "../components/smart_connect/smart_connect.h"
#include "../components/data_transmission/data_transmission.h"
#include "../components/sleep_time/sleep_time.h"
#include "../components/sd_card/sd_card.h"


static const char *TAG = "main";





// TODO - pinout issue: SENSOR_VN, SENSOR_VP, and IO34 cannot be configured as outputs
//						external jumper (wire) IO34 to IO33
//						external jumper (use resistor) SENSOR_VN to IO14 (alarm 1)
//						external jumper (use resistor) SENSOR_VP to IO12 (alarm 2/wifi reset button)
//						configure SENSOR_VN and SENSOR_VP as inputs
//						configure IO_14 and IO_12 as outputs with series resistance


// Assembling info for a POST request19
// TODO - go back through and make sure that all memory problems are completely solved.
void send_all_POST(){
	
	// MAX SIZE	 	orig	appended
	/*
		timestamp 	11		26
		readable	15		34
		device ID	18		28
		event type	26		38
	*/
	
	int n = 2;
	int i = 0;
	// unix timestamp
	uint64_t temp_time_num;
	time_t temp_time_num_t;
	char temp_time[11];
	char post_time[26] = "unix_timestamp=";
	memset(temp_time,'\0',sizeof(temp_time));
	
	// readable timestamp
	char temp_time_readable[15];
	char post_time_human[34] = "readable_timestamp=";
	memset(temp_time_readable,'\0',sizeof(temp_time_readable));
	
	// device ID
	char unique_id[18];
	char post_mac[28] = "device_id=";
	memset(unique_id,'\0',sizeof(unique_id));
	
	// event type
	char event_type[26];
	char post_type[38] = "request_type=";
	memset(event_type,'\0',sizeof(event_type));
	
	// buffer to hold the contents of a line needing parsing
	char buffer[LINE_BUFFER_SIZE];
	char response_buff[RESPONSE_BUFFER_SIZE];
	
	// in order to treat all events in the log the same, re-create MAC and pretty timestamp
	getMacAddress(unique_id);
	strcat(post_mac,unique_id);
	// for each line in UNSNT.txt, parse into associated data fields and send
	// if the buffer comes back NULL, 0 is returned and the text file is done
	while(sd_read_line_unsent(buffer,n)){

		// the first 10 characters are from the unix timestamp
		memcpy(temp_time, buffer,10);
		strcat(post_time, temp_time);
		//convert to readable format
		temp_time_num = (uint64_t)atoi(temp_time);
		temp_time_num_t = (time_t)temp_time_num;
		strftime (temp_time_readable,27,"%D/%R",gmtime(&temp_time_num_t));
		strcat(post_time_human, temp_time_readable);		
		
		// search buffer until '~' is reached and store remaining characters in event_type
		while((i<LINE_BUFFER_SIZE)&&(buffer[i]!='~')){
			i++;
		}
		// if post type is not found, make note of this (prevent buffer overflow)
		if(buffer[i]!='~'){
			strcat(post_type,"UNKNOWN");
		}
		else{
			memcpy(event_type, buffer+i+1, 25);
			strcat(post_type, event_type);
		}
		// assemble and send post request to server
		http_POST(post_time, post_time_human, post_mac, post_type);
		
		// clean up for next POST
		strcpy(post_time, "unix_timestamp=");
		strcpy(post_time_human, "readable_timestamp=");
		strcpy(post_type,"request_type=");
		
		// go to next line of file
		n++;
	}
	
	// get alarm instructions
	http_POST_get_instructions(post_mac, response_buff, RESPONSE_BUFFER_SIZE);
	ESP_LOGI(TAG, "Instructions: %s", response_buff);
	
	// clean up
	sd_clear_unsent();	
}


// log process or event in SD card
/*	(event type)			(value)		(where to log)
	EVENT_CAP_TURN			0 			UNSNT and CPLOG
	EVENT_GOOD_CONNECTION 	1			PRLOG
	EVENT_BAD_CONNECTION	2			UNSNT and PRLOG
	EVENT_ALARM_TRIGGERED	3			UNSNT and PRLOG
	EVENT_FIRST_WAKEUP		4			PRLOG
*/
void log_activity_SD(int event_type){
	
	int timeofday = 0;
    char timestamp[50];
	char timestamp_pretty[50];
	char unique_id[50];
	char post_type[50];
	
	//get the current time as a unix timestamp and as human readable
	timeofday = get_current_time_s();
	ESP_LOGI(TAG, "unix time: %d", timeofday);
	itoa(timeofday, timestamp, 10);
	get_current_time_pretty(timestamp_pretty);
	ESP_LOGI(TAG, "pretty time: %s", timestamp_pretty);
	//get a unique id (MAC address) from BLK0 in EFUSE
	getMacAddress(unique_id);
	
	switch(event_type){
		
		// post_type should always take the form "~EVENT_TYPE"
		// start with a ~, no spaces
		case EVENT_CAP_TURN:
			strcpy(post_type, "~CAP_TURN");
			sd_log_event(timestamp, timestamp_pretty, unique_id, post_type);
			break;
		case EVENT_GOOD_CONNECTION:
			strcpy(post_type, "~SUCCESSFUL_TRANSMISSION");
			sd_log_process(timestamp, post_type, 0);
			break;
		case EVENT_BAD_CONNECTION:
			strcpy(post_type, "~UNSUCCESSFUL_TRANSMISSION");
			sd_log_process(timestamp, post_type, 1);
			break;
		case EVENT_ALARM_TRIGGERED:
			strcpy(post_type, "~ALARM_TRIGGERED");
			sd_log_process(timestamp, post_type, 0);
			break;
		case EVENT_FIRST_WAKEUP:
			strcpy(post_type, "~FIRST_WAKEUP");
			sd_log_process(timestamp, post_type, 1);
			break;
		default:
			break;
	}
	
}







void app_main()
{
	int state = 0;
	// initialize
	ESP_ERROR_CHECK( nvs_flash_init() );
	wake_up_routine();
	// enable SD card by setting IO32 high
	// gpio_pad_select_gpio(IO32);
	// gpio_set_direction(IO32, GPIO_MODE_OUTPUT);
	// gpio_set_level(IO32, 1);
	sd_card_init();
	// state machine behavior
	state = current_state();
	switch(state){
		
		case INITIAL_WAKE:
			ESP_LOGI(TAG, "Initial wake");
			// set up file management system in SD card
			sd_create_new_file();
			vTaskDelay(1000 / portTICK_PERIOD_MS);
			// disable timed reconnect
			set_future_state(WIFI_RESET);
			go_to_sleep();
			break;
		case CAP_DETECT:
			ESP_LOGI(TAG, "Cap detect");
			log_activity_SD(EVENT_CAP_TURN);
			vTaskDelay(1000 / portTICK_PERIOD_MS);
			initialise_wifi(TIMED_RECONNECT);
			ESP_LOGI(TAG, "initialized wifi and ready to post");
			// wait for either a connection to be made or an error to occur
			// if the if statement is evaluated at 0, the AP could not be found or connected to
			if(app_wifi_wait_connected()){
				log_activity_SD(EVENT_GOOD_CONNECTION);
				ESP_LOGI(TAG, "reconnected to WiFi");
				send_all_POST();
			}
			else{
				log_activity_SD(EVENT_BAD_CONNECTION);
			}
			vTaskDelay(1000 / portTICK_PERIOD_MS);
			go_to_sleep();
			break;
		case TIMED_RECONNECT:
			// TODO - check battery status
			// TODO - properly retrieve instructions
			// Leads to states WIFI_BAD, WIFI_GOOD, or ENGAGE_ALARM
			ESP_LOGI(TAG, "Wakeup cause - timed reconnect");
			initialise_wifi(TIMED_RECONNECT);
			ESP_LOGI(TAG, "initialized wifi and ready to post");
			// wait for either a connection to be made or an error to occur
			// if the if statement is evaluated at 0, the AP could not be found or connected to
			if(app_wifi_wait_connected()){
				// frequently reset system time
				//calibrate_time();
				// log in SD card
				log_activity_SD(EVENT_GOOD_CONNECTION);
				ESP_LOGI(TAG, "reconnected to WiFi");
				send_all_POST();
				set_future_state(CAP_DETECT);
				init_sleep_time();
			}
			else{
				log_activity_SD(EVENT_BAD_CONNECTION);
			}
			vTaskDelay(1000 / portTICK_PERIOD_MS);
			go_to_sleep();
			break;
		case TIMED_CLOCK_RESET:
			ESP_LOGI(TAG, "midpoint wake-up");
			go_to_sleep();
			break;
		case EN_CAP_DETECT:
			// TODO - remove IO34 (cap turn) as a wakeup source....
			ESP_LOGI(TAG, "enable cap detect");
			go_to_sleep();
			break;
		case WIFI_RESET:
			// future state will continue to be WIFI_RESET until we have a good connection
			set_future_state(WIFI_RESET);
			ESP_LOGI(TAG, "Wifi Reset");
			initialise_wifi(INITIAL_WAKE);
			// if we have a good connetion, go through posting and logging, 
			if(app_wifi_wait_connected()){
				calibrate_time();
				vTaskDelay(1000 / portTICK_PERIOD_MS);
				log_activity_SD(EVENT_FIRST_WAKEUP);
				ESP_LOGI(TAG, "connected to WiFi");
				send_all_POST();
				// make sure future state is not WIFI_RESET
				// future state may be reset later by a different function
				set_future_state(CAP_DETECT);
				init_sleep_time();
			}
			vTaskDelay(1000 / portTICK_PERIOD_MS);
			go_to_sleep();
			break;
		case SLEEP:
			go_to_sleep();
		case ENGAGE_ALARM:
			ESP_LOGI(TAG, "Engaging alarm behavior");
			// LED is toggled when device finishes going through wake_up_routine()
			// TODO - add buzzer
			// delay keeps LED high, shutting off automatically turns it off
			buzzer_config();
			vTaskDelay(1000 / portTICK_PERIOD_MS);
			// reseting alarm time is done in sleep_time
			go_to_sleep();
		default:
			break;
	}

}
